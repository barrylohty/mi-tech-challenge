package com.barryloh.mitechchallenge.network;

/**
 * Created by barry on 15/05/2018.
 */

public enum APIEndpoint {

    UNDEFINED("undefined"),
    DELIVERIES("deliveries");

    private final String value;

    APIEndpoint(String value) {
        this.value = value;
    }

    /**
     * Returns representation of string value.
     * @return Get representation of string value.
     */
    public String getValue() {
        return value;
    }

    /**
     * Revert from value to enum.
     * @param value Represented string value
     * @return App connection enum object
     */
    public static APIEndpoint fromString(String value) {
        for (APIEndpoint ac : APIEndpoint.values()) {
            if (ac.getValue().equalsIgnoreCase(value)) {
                return ac;
            }
        }

        return UNDEFINED;
    }

}
