package com.barryloh.mitechchallenge.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by barry on 15/05/2018.
 */

public class LocationResponse {

    @SerializedName("lat")
    public double latitude;

    @SerializedName("lng")
    public double longitude;

    public String address;

    public LocationResponse() {}

    public LocationResponse(double latitude, double longitude, String address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
    }

}
