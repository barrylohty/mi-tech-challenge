package com.barryloh.mitechchallenge.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.barryloh.mitechchallenge.R;
import com.barryloh.mitechchallenge.callbacks.RecyclerViewCallbacks;
import com.barryloh.mitechchallenge.database.models.Delivery;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by barry on 15/05/2018.
 */

public class DeliveryViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {

    @BindView(R.id.map)
    MapView mapView;

    @BindView(R.id.view_background_gradient)
    View vBackgroundGradient;

    @BindView(R.id.txt_description)
    TextView txtDescription;

    @BindView(R.id.txt_address)
    TextView txtAddress;

    private GoogleMap googleMap;
    private LatLng mapCenterCoordinates;

    private Context context;

    public DeliveryViewHolder(Context context, View itemView) {
        super(itemView);
        this.context = context;
        ButterKnife.bind(this, itemView);

        mapView.setClickable(false);
        mapView.onCreate(null);
        mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(context, R.raw.custom_map_style)
        );

        MapsInitializer.initialize(context);
        googleMap.getUiSettings().setMapToolbarEnabled(false);

        if (mapCenterCoordinates != null) {
            updateMapContents();
        }
    }

    /**
     * Set data to the card
     * @param delivery
     */
    public void setData(Delivery delivery) {
        txtDescription.setText( delivery.getDescription() );
        txtAddress.setText( delivery.getAddress() );
        mapCenterCoordinates = delivery.getCoordinate();

        if (googleMap != null) {
            updateMapContents();
        }
    }

    /**
     * Bind On Click callbacks to the card.
     * @param delivery
     * @param callback
     */
    public void bind(final Delivery delivery, final RecyclerViewCallbacks callback) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.onItemClick(itemView, delivery);
                }
            }
        });
    }

    private void updateMapContents() {
        // Since the mapView is re-used, need to remove pre-existing mapView features.
        googleMap.clear();

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(mapCenterCoordinates, 16f);
        googleMap.moveCamera(cameraUpdate);
    }

    public void setFirstBackgroundGradient() {
        vBackgroundGradient.setBackgroundResource(R.drawable.db_background_delivery_card);
    }

    public void setSecondBackgroundGradient() {
        vBackgroundGradient.setBackgroundResource(R.drawable.db_background_delivery_card_2);
    }

    public void setThirdBackgroundGradient() {
        vBackgroundGradient.setBackgroundResource(R.drawable.db_background_delivery_card_3);
    }

}
