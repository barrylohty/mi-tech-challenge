package com.barryloh.mitechchallenge.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by barry on 15/05/2018.
 */

public class DeliveryResponse {

    public String description;

    @SerializedName("imageURL")
    public String imageURL;

    public LocationResponse location;

    public DeliveryResponse() {}

    public DeliveryResponse(String description, String imageURL, LocationResponse location) {
        this.description = description;
        this.imageURL = imageURL;
        this.location = location;
    }

}
