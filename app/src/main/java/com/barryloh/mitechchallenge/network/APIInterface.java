package com.barryloh.mitechchallenge.network;

import com.barryloh.mitechchallenge.models.response.DeliveryResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by barry on 15/05/2018.
 */

public interface APIInterface {

    @GET(API.ENDPOINT_DELIVERIES)
    Call<List<DeliveryResponse>> getDeliveries();

}
