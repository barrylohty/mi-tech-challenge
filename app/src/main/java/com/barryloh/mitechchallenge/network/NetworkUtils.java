package com.barryloh.mitechchallenge.network;

import android.app.AlertDialog;
import android.content.Context;

import com.barryloh.mitechchallenge.R;

/**
 * Created by barry on 16/05/2018.
 */

public class NetworkUtils {

    public static void showErrorMessage(Context context, int responseCode, String message) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.dialog_title_network_error)
                .setMessage("(" + responseCode + ") " + message)
                .setPositiveButton(R.string.dialog_button_ok, null)
                .create()
                .show();
    }

}
