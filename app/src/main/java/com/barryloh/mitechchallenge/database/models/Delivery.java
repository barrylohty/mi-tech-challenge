package com.barryloh.mitechchallenge.database.models;

import com.barryloh.mitechchallenge.database.config.DatabaseManager;
import com.barryloh.mitechchallenge.models.response.DeliveryResponse;
import com.barryloh.mitechchallenge.models.response.LocationResponse;
import com.barryloh.mitechchallenge.utils.statics.Keys;
import com.google.android.gms.maps.model.LatLng;

import org.parceler.Parcel;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by barry on 15/05/2018.
 */

@Parcel
public class Delivery extends RealmObject {

    // General details of the delivery.
    private String description;
    private String imageURL;

    // Location details.
    private double latitude;
    private double longitude;
    private String address;

    // TS details.
    private Date createdAt;

    /**
     * Check if internal database has a record with said description.
     * @param description
     * @return
     */
    public static boolean hasRecordWithDescription(String description) {
        return getByDescription(description) != null;
    }

    /**
     * Gets record by description. Must be an exact match!
     * @param description
     * @return
     */
    public static Delivery getByDescription(String description) {
        return DatabaseManager.getRealm()
                .where(Delivery.class)
                .equalTo(Keys.DESCRIPTION, description)
                .findFirst();
    }

    /**
     * Gets all active records sorted by the latest data first
     * @return
     */
    public static List<Delivery> getAll() {
        Realm realm = DatabaseManager.getRealm();
        RealmResults<Delivery> results = realm.where(Delivery.class)
                .sort(Keys.CREATED_AT, Sort.DESCENDING)
                .findAll();

        // Return an immutable copy of the results.
        return realm.copyFromRealm(results);
    }

    /**
     * Creates a new object and save to internal database storage.
     * @param response DeliveryResponse model from API
     * @return Immutable created model
     */
    public static Delivery create(DeliveryResponse response) {
        Realm realm = DatabaseManager.getRealm();

        realm.beginTransaction();
        Delivery delivery = realm.createObject(Delivery.class);
        delivery.setDescription(response.description);
        delivery.setImageURL(response.imageURL);
        delivery.setLocation(response.location);
        realm.commitTransaction();

        return realm.copyFromRealm(delivery);
    }

    public Delivery() {}

    public Delivery(String description, String imageURL, LocationResponse location) {
        this.description = description;
        this.imageURL = imageURL;
        this.latitude = location.latitude;
        this.longitude = location.longitude;
        this.address = location.address;
        this.createdAt = new Date();
    }

    public String getDescription() {
        return description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    /**
     * Returns a `LatLng` object from given latitude and longitude.
     * @return
     */
    public LatLng getCoordinate() {
        return new LatLng(latitude, longitude);
    }

    public String getAddress() {
        return address;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public void setLocation(LocationResponse location) {
        this.latitude = location.latitude;
        this.longitude = location.longitude;
        this.address = location.address;
    }

}
