package com.barryloh.mitechchallenge.database.config;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by barry on 20/03/2018.
 */

public class DatabaseManager {

    private static Realm realm;
    private static RealmConfiguration configuration;

    public static Realm getRealm() {
        if (realm == null || realm.isClosed()) {
            realm = Realm.getInstance(configuration);
        }

        return realm;
    }

    public static void close() {
        if (realm != null) {
            realm.close();
        }
    }

    public static void initializeRealmConfiguration(Context context) {
        if (configuration == null) {
            setRealmConfiguration(
                    DatabaseConfiguration.generate()
            );
        }
    }

    public static void setRealmConfiguration(RealmConfiguration configuration) {
        DatabaseManager.configuration = configuration;
        Realm.setDefaultConfiguration(configuration);
    }

}
