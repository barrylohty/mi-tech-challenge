package com.barryloh.mitechchallenge.network;

import android.content.Context;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by barry on 15/05/2018.
 */

public class API {

    // Maintain other environment URL here.
    public static final String BASE_URL_STAGING = "https://staging.massiveinfinity.com/api/";

    // References the active environment URL to be used in the app.
    public static String BASE_URL = BASE_URL_STAGING;

    // Endpoints maintained here!
    public static final String ENDPOINT_DELIVERIES = "deliveries";

    private static Retrofit retrofit = null;

    /**
     * Generates the service which is used to perform a REST call documented in `APIInterface` class.
     * @param context Application state
     * @return API service
     */
    public static APIInterface generate(Context context) {
        return getClient(context).create(APIInterface.class);
    }

    /**
     * Returns the Retrofit client responsible to interface the service which contains all the documented REST calls.
     * This method also caches the client to prevent recreating it at every call.
     *
     * @param context Application state
     * @return Cached Retrofit client
     */
    private static Retrofit getClient(Context context) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }

}
