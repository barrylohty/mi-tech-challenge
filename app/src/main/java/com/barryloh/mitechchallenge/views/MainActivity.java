package com.barryloh.mitechchallenge.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;

import com.barryloh.mitechchallenge.R;
import com.barryloh.mitechchallenge.callbacks.NetworkCallbacks;
import com.barryloh.mitechchallenge.database.models.Delivery;
import com.barryloh.mitechchallenge.models.response.DeliveryResponse;
import com.barryloh.mitechchallenge.network.APIEndpoint;
import com.barryloh.mitechchallenge.network.NetworkUtils;
import com.barryloh.mitechchallenge.utils.statics.Keys;
import com.barryloh.mitechchallenge.viewmodels.DeliveryViewModel;
import com.barryloh.mitechchallenge.views.base.BaseActivity;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;

import static com.barryloh.mitechchallenge.utils.statics.Logger.LOGD;
import static com.barryloh.mitechchallenge.utils.statics.Logger.LOGE;

public class MainActivity extends BaseActivity implements NetworkCallbacks<List<DeliveryResponse>> {

    @Nullable
    @BindView(R.id.frame_navigation)
    FrameLayout frameNavigation;

    @Nullable
    @BindView(R.id.frame_details)
    FrameLayout frameDetails;

    // Keep in reference of the two most important Fragments in the app!
    private DeliveryFragment deliveryFragment;
    private DeliveryDetailsFragment deliveryDetailsFragment;

    // The VM responsible for getting data.
    private DeliveryViewModel viewModel;

    // Immutable static tags used to reference the fragments.
    private static final String TAG_FRAGMENT_DELIVERY_LIST = "DELIVERY_LIST";
    private static final String TAG_FRAGMENT_DELIVERY_DETAILS = "DELIVERY_DETAILS";

    @Override
    protected int setContentViewLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        LOGD("## frame details:" + isTabletLandscapeLayoutActive());
        // Manage state of when the device changes orientation.
        // When the device changes orientation, the Main Activity is recreated, hence it is checked whether
        // to preserve the fragment layout states or reinitialise them.
        if (savedInstanceState == null) {
            // Initialize the View Model upon entering the activity initially.
            // This can be done here as the View Model is being shared and does not want to be reinitalized.
            viewModel = new DeliveryViewModel(this, this);

            // Initialise both fragments if layout is supported.
            initDeliveryListFragment();

            if ( isTabletLandscapeLayoutActive() ) {
                initDeliveryDetailsFragment();
            }
        }
        // If the device changes orientation, prevent layouts from getting reinitialised. Instead, reuse it.
        else {
            FragmentManager fragmentManager = getSupportFragmentManager();
            deliveryFragment = (DeliveryFragment) fragmentManager.findFragmentByTag(TAG_FRAGMENT_DELIVERY_LIST);
            deliveryDetailsFragment = (DeliveryDetailsFragment) fragmentManager.findFragmentByTag(TAG_FRAGMENT_DELIVERY_DETAILS);

            // If the landscape layout is active and fragment is not initialised yet, do it now!
            if ( isTabletLandscapeLayoutActive() && deliveryDetailsFragment == null) {
                initDeliveryDetailsFragment();
            }
        }
    }

    @Override
    public void onNetworkSuccess(APIEndpoint endpoint, List<DeliveryResponse> response) {
        switch (endpoint) {
            case DELIVERIES:
                // Note: Makes a lot of reference to the Delivery List Fragment to manage the different states.
                if (response.size() == 0) {
                    deliveryFragment.onEmptyResponse();
                    return;
                }

                viewModel.addToListFromNetwork(response);
                if (deliveryFragment != null) {
                    deliveryFragment.onNetworkSuccess();
                }

                if (deliveryDetailsFragment != null) {
                    deliveryDetailsFragment.loadAllDeliveryLocationMarkers();
                }
                break;
        }
    }

    @Override
    public void onNetworkFailure(APIEndpoint endpoint, int responseCode, String errorMessage) {
        switch (endpoint) {
            case DELIVERIES:
                LOGE("An error (" + responseCode + ") has occurred with message: " + errorMessage);
                deliveryFragment.onNetworkFailure();
                NetworkUtils.showErrorMessage(MainActivity.this, responseCode, errorMessage);
                break;
        }
    }

    /**
     * When a delivery card is clicked, this method should be called.
     * This will manage whether to show a new activity or highlight the delivery on the other Fragment (during multi-pane layout).
     * @param delivery Delivery data being clicked
     */
    public void onSelectDeliveryInfo(Delivery delivery) {
        if ( isTabletLandscapeLayoutActive() ) {
            deliveryDetailsFragment.showDeliveryDetails(delivery);
        } else {
            Bundle args = new Bundle();
            args.putParcelable(Keys.BUNDLE_DELIVERY, Parcels.wrap(delivery));

            Intent intent = new Intent(this, DeliveryDetailsActivity.class);
            intent.putExtras(args);
            startActivity(intent);
        }
    }

    /**
     * Check whether the multi-pane layout is active or not.
     * This is only used for landscape orientation for tablets.
     * @return Yes if active
     */
    private boolean isTabletLandscapeLayoutActive() {
        return frameDetails != null;
    }

    /**
     * Initialise the Delivery List Fragment.
     */
    private void initDeliveryListFragment() {
        deliveryFragment = DeliveryFragment.newInstance(viewModel);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_navigation, deliveryFragment, TAG_FRAGMENT_DELIVERY_LIST);
        fragmentTransaction.commit();
    }

    /**
     * Initialise the Delivery Details Fragment.
     */
    private void initDeliveryDetailsFragment() {
        deliveryDetailsFragment = DeliveryDetailsFragment.newInstance();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_details, deliveryDetailsFragment, TAG_FRAGMENT_DELIVERY_DETAILS);
        fragmentTransaction.commit();
    }

}
