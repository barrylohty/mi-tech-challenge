package com.barryloh.mitechchallenge.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.barryloh.mitechchallenge.R;
import com.barryloh.mitechchallenge.callbacks.RecyclerViewCallbacks;
import com.barryloh.mitechchallenge.database.models.Delivery;
import com.barryloh.mitechchallenge.viewholders.DeliveryViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by barry on 15/05/2018.
 */

public class DeliveryAdapter extends RecyclerView.Adapter<DeliveryViewHolder> {

    private Context context;

    private List<Delivery> deliveries;
    private final RecyclerViewCallbacks callback;

    public DeliveryAdapter(Context context, RecyclerViewCallbacks callback) {
        this.context = context;
        deliveries = new ArrayList<>();
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return deliveries.size();
    }

    @NonNull
    @Override
    public DeliveryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( parent.getContext() )
                .inflate(R.layout.list_item_delivery, parent, false);

        return new DeliveryViewHolder(context, view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeliveryViewHolder holder, int position) {
        Delivery delivery = deliveries.get(position);
        holder.setData(delivery);
        holder.bind(delivery, callback);

        // Just add a little spice to the background color by the position.
        // Because position starts at 0, it is added by 1 here to indicate first, second or third position.
        // This can be randomized later on or standardized to one color, or even by region!
        if ((position + 1) % 2 == 0) {
            holder.setSecondBackgroundGradient();
        } else if ((position + 1) % 3 == 0) {
            holder.setThirdBackgroundGradient();
        } else {
            holder.setFirstBackgroundGradient();
        }
    }

    /**
     * Add a new `delivery` model object into the list.
     * It will be appended at the end of the list.
     * @param delivery New model
     */
    public void addDelivery(Delivery delivery) {
        this.deliveries.add(delivery);

        // Notify item inserted at the end of the list.
        // Note that first position is 0.
        notifyItemInserted(this.deliveries.size() - 1);
    }

    /**
     * Add two or more `delivery` model objects simultaneously into the list.
     * They will be appended at the end of the list.
     * @param deliveries New models
     */
    public void addDeliveries(List<Delivery> deliveries) {
        notifyItemRangeInserted(this.deliveries.size(), deliveries.size());
        this.deliveries.addAll(deliveries);
    }

}
