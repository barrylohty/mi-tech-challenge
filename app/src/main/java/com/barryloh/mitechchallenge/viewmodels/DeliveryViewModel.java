package com.barryloh.mitechchallenge.viewmodels;

import android.content.Context;

import com.barryloh.mitechchallenge.adapters.DeliveryAdapter;
import com.barryloh.mitechchallenge.callbacks.NetworkCallbacks;
import com.barryloh.mitechchallenge.database.config.DatabaseManager;
import com.barryloh.mitechchallenge.database.models.Delivery;
import com.barryloh.mitechchallenge.models.response.DeliveryResponse;
import com.barryloh.mitechchallenge.network.APIEndpoint;
import com.barryloh.mitechchallenge.viewmodels.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;

/**
 * A VM class to contain logic of querying data from local database and network.
 * Also pre-processes new data from network.
 *
 * Created by barry on 15/05/2018.
 */

public class DeliveryViewModel extends BaseViewModel {

    public DeliveryViewModel(Context context, NetworkCallbacks callback) {
        super(context, callback);
    }

    /**
     * Initialize the list of deliveries with the ones saved in local database.
     * @return A flag of whether the list is empty or not.
     */
    public boolean initializeDeliveriesFromLocal() {
        List<Delivery> deliveries = Delivery.getAll();
        getAdapter().addDeliveries(deliveries);

        return deliveries.size() == 0;
    }

    /**
     * Makes a GET call to the API endpoint to query all deliveries from server.
     */
    public void getAllDeliveries() {
        Call<List<DeliveryResponse>> call = getAPIClient().getDeliveries();
        enqueueCall(call, APIEndpoint.DELIVERIES);
    }

    /**
     * This method compares all the responses received from server with local database.
     * If it is not found in local database, the new record will be created and added to local database and list to display immediately.
     *
     * However, there are assumptions going to be made here:
     * 1. The list response received from server is not very huge.
     * 2. No deletion of record will need to be performed.
     *
     * @param deliveries New delivery list from server.
     */
    public void addToListFromNetwork(List<DeliveryResponse> deliveries) {
        List<Delivery> newDeliveries = new ArrayList<>();

        // Save the responses to local storage first.
        for (DeliveryResponse response : deliveries) {

            // Check first if the record is already found on screen.
            // TODO: Determine uniqueness by some ID instead of the description.
            // However, since an ID is not being returned, the `description` will be assumed as unique.
            if ( !Delivery.hasRecordWithDescription(response.description) ) {
                Delivery newDelivery = Delivery.create(response);
                newDeliveries.add(newDelivery);
            }
        }

        if (newDeliveries.size() > 0) {
            addToList(newDeliveries);
        }
    }

    /**
     * Add delivery models to Recycler View list.
     * @param deliveries Delivery models to add to list
     */
    private void addToList(List<Delivery> deliveries) {
        getAdapter().addDeliveries(deliveries);
    }

    /**
     * Get the adapter attached to the Recycler View.
     * @return DeliveryAdapter
     */
    private DeliveryAdapter getAdapter() {
        return (DeliveryAdapter) recyclerView.getAdapter();
    }

}
