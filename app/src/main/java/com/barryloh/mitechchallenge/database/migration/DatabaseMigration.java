package com.barryloh.mitechchallenge.database.migration;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

import static com.barryloh.mitechchallenge.utils.statics.Logger.LOGD;

/**
 * Created by barry on 05/04/2018.
 */

public class DatabaseMigration implements RealmMigration {

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        LOGD("## old: " + oldVersion + "; new: " + newVersion);

        // DynamicRealm exposes an editable schema
        RealmSchema schema = realm.getSchema();

        if (oldVersion == 1) {
            // TODO: Perform migration of schema here.

            oldVersion++;
        }
    }

}
