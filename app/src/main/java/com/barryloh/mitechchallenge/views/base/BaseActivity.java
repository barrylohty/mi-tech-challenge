package com.barryloh.mitechchallenge.views.base;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.barryloh.mitechchallenge.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.barryloh.mitechchallenge.utils.statics.Logger.LOGD;

/**
 * Created by barry on 15/05/2018.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Nullable
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    /**
     * Declare the activity layout's resource file.
     * @return
     */
    protected abstract int setContentViewLayout();

    /**
     * Override this method to declare the title to display in the toolbar.
     * @return
     */
    protected String setToolbarTitle() { return ""; }

    /**
     * Override this method to perform any other initializing configurations.
     */
    protected void init(Bundle savedInstanceState) {}

    /**
     * Override this method to obtain arguments.
     * @param args
     */
    protected void getArguments(Bundle args) {}

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setContentViewLayout());
        ButterKnife.bind(this);

        initToolbar();

        // Hardcoded here as to specify a grey Status Bar. Flat design FTW!
        getWindow().setStatusBarColor(
                getCompatColor(R.color.statusBarAlternative)
        );

        Bundle args = getIntent().getExtras();
        if (args != null) {
            getArguments(args);
        }

        init(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Get color from resource with AppCompat
     * @param id
     * @return
     */
    protected int getCompatColor(int id) {
        return ContextCompat.getColor(BaseActivity.this, id);
    }

    /**
     * Get drawable from resource with AppCompat
     * @param id
     * @return
     */
    protected Drawable getCompatDrawable(int id) {
        return ContextCompat.getDrawable(BaseActivity.this, id);
    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(setToolbarTitle());
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
            }
        }
    }

    protected final void removeToolbarNavigation() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }
    }

}
