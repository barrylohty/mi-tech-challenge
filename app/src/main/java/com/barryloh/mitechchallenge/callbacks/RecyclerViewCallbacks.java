package com.barryloh.mitechchallenge.callbacks;

import android.view.View;

/**
 * Created by barry on 15/05/2018.
 */

public interface RecyclerViewCallbacks<T> {

    void onItemClick(View view, T data);

}
