package com.barryloh.mitechchallenge.views;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.barryloh.mitechchallenge.R;
import com.barryloh.mitechchallenge.database.models.Delivery;
import com.barryloh.mitechchallenge.views.base.BaseFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static android.content.Intent.ACTION_VIEW;

/**
 * Created by barry on 15/05/2018.
 */

public class DeliveryDetailsFragment extends BaseFragment implements OnMapReadyCallback {

    @BindView(R.id.map)
    MapView mapView;

    @BindView(R.id.card_details)
    CardView cardDetails;

    @BindView(R.id.txt_description)
    TextView txtDescription;

    @BindView(R.id.txt_address)
    TextView txtAddress;

    // Holds reference to the GoogleMap object for Class usage.
    private GoogleMap googleMap;
    private LatLng mapCenterCoordinates;

    // Holds reference to the active `Delivery` model object.
    private Delivery delivery;

    private static final double DEFAULT_SINGAPORE_LATITUDE = 1.290270;
    private static final double DEFAULT_SINGAPORE_LONGITUDE = 103.851959;
    private static final float DEFAULT_ZOOM_SINGAPORE = 11f;
    private static final float DEFAULT_ZOOM_DELIVERY_LOCATION = 16f;
    private static final int DEFAULT_CAMERA_ANIMATION_DURATION = 500;      // In milliseconds.

    @OnClick(R.id.button_delivered)
    public void onClickButtonDelivered() {
        // TODO: Actually make an API call of some sort to mark this package as delivered.
        // Right now this function does not do anything significant. Only a POC to show further
        // capabilities and potential functionalities of the app.
        new AlertDialog.Builder(context)
                .setTitle(R.string.delivery_dialog_title_delivered)
                .setPositiveButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Snackbar.make(
                                context.findViewById(android.R.id.content),
                                R.string.delivery_snackbar_delivered_success,
                                Snackbar.LENGTH_LONG
                        ).show();
                    }
                })
                .setNegativeButton(R.string.dialog_button_no, null)
                .create()
                .show();
    }

    @OnClick(R.id.button_navigate)
    public void onClickButtonNavigate() {
        if (delivery != null) {
            Intent intent = new Intent(
                    ACTION_VIEW,
                    Uri.parse("google.navigation:q=" + delivery.getLatitude() + "," + delivery.getLongitude())
            );
            startActivity(intent);
        } else {
            Snackbar.make(
                    context.findViewById(android.R.id.content),
                    R.string.delivery_snackbar_navigate_failure,
                    Snackbar.LENGTH_LONG
            ).show();
        }
    }

    /**
     * Default static method to create an instance of this Fragment without defining the `Delivery` model object.
     * Use this if being created for multi-pane layout in tablet.
     * @return This Fragment instance
     */
    public static DeliveryDetailsFragment newInstance() {
        return new DeliveryDetailsFragment();
    }

    /**
     * Creates an instance with a defined `Delivery` model object. The map will automatically highlight
     * the defined model object.
     * @param delivery Active model
     * @return This Fragment instance
     */
    public static DeliveryDetailsFragment newInstance(Delivery delivery) {
        DeliveryDetailsFragment fragment = new DeliveryDetailsFragment();

        // Just don't do anything if its null.
        if (delivery != null) {
            fragment.setDelivery(delivery);
        }

        return fragment;
    }

    @Override
    protected int inflateLayout() {
        return R.layout.fragment_delivery_details;
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        mapView.setClickable(false);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Perform initialising of Google Map object and any additional configurations.
        this.googleMap = googleMap;
        MapsInitializer.initialize(context);

        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setAllGesturesEnabled(false);

        // Re-update map contents to its initial state.
        updateInitialMapContents();

        // Since this fragment is being used for normal layouts as well, check if the `Delivery` model has been defined.
        // If it has been defined, then zoom to that location and add a marker.
        if (delivery != null) {
            addMarkerToMap( delivery.getCoordinate() );
            showDeliveryDetails(delivery);
        }
        // Else, load all possible markers onto map.
        else {
            loadAllDeliveryLocationMarkers();
        }
    }

    /**
     * Show the delivery details onto screen.
     * This would include the already available `description`, `address` and CTA buttons.
     * @param delivery Active model object
     */
    public void showDeliveryDetails(Delivery delivery) {
        setDelivery(delivery);
        cardDetails.setVisibility(View.VISIBLE);

        // Override the center coordinates for zooming into the map.
        mapCenterCoordinates = delivery.getCoordinate();
        setZoomToDeliveryLocation();

        txtDescription.setText( delivery.getDescription() );
        txtAddress.setText( delivery.getAddress() );
    }

    /**
     * This method is being called whenever being used in tablet landscape layout.
     * It is being used to populate markers across the map for all loaded delivery locations.
     */
    public void loadAllDeliveryLocationMarkers() {
        // Sometimes the Map has not finished loaded before the network API is successful.
        // If that happens, the app will crash due  to `googleMap` object being null.
        // Just add a checking to prevent app from crashing. If network API is loaded successfully before Map loads,
        // the new data would be loaded into database already.
        // Hence once the Map loads, this method will be called upon initialisation.
        if (googleMap == null) {
            return;
        }

        delivery = null;
        updateInitialMapContents();
        List<Delivery> deliveries = Delivery.getAll();

        for (Delivery delivery : deliveries) {
            addMarkerToMap( delivery.getCoordinate() );
        }
    }

    /**
     * Sets the Delivery model for this Fragment to hold reference to it.
     * Not used for tablet landscape layouts.
     * @param delivery
     */
    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    /**
     * Reinitalise the map content and initial zoom view.
     */
    private void updateInitialMapContents() {
        // Since the mapView is re-used, need to remove pre-existing mapView features.
        googleMap.clear();

        // Hide the card details until more details needed.
        cardDetails.setVisibility(View.GONE);

        // Default initial coordinate to Singapore.
        mapCenterCoordinates = new LatLng(DEFAULT_SINGAPORE_LATITUDE, DEFAULT_SINGAPORE_LONGITUDE);

        // Default map to zoom to SG.
        if (delivery == null) {
            setZoomToSingapore();
        }
    }

    /**
     * Set map zoomed to Singapore. The zoom factor has been fixed and calculated to show the island of SG.
     */
    private void setZoomToSingapore() {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(mapCenterCoordinates, DEFAULT_ZOOM_SINGAPORE);
        googleMap.animateCamera(cameraUpdate, DEFAULT_CAMERA_ANIMATION_DURATION, null);
    }

    /**
     * Set map zoomed to a delivery location. Zoom factor can be changed to make it appear nearer, or further.
     */
    private void setZoomToDeliveryLocation() {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(mapCenterCoordinates, DEFAULT_ZOOM_DELIVERY_LOCATION);
        googleMap.animateCamera(cameraUpdate, DEFAULT_CAMERA_ANIMATION_DURATION, null);
    }

    /**
     * Add marker to map.
     * Use this method wisely as repeated markers can be added. No validations being performed.
     * @param coordinate Coordinate in `LatLng` object
     */
    private void addMarkerToMap(LatLng coordinate) {
        MarkerOptions marker = new MarkerOptions();
        marker.position(coordinate);

        googleMap.addMarker(marker);
    }

}
