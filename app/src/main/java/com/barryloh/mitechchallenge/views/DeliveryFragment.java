package com.barryloh.mitechchallenge.views;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.LinearLayout;

import com.barryloh.mitechchallenge.R;
import com.barryloh.mitechchallenge.adapters.DeliveryAdapter;
import com.barryloh.mitechchallenge.callbacks.RecyclerViewCallbacks;
import com.barryloh.mitechchallenge.database.models.Delivery;
import com.barryloh.mitechchallenge.tools.SimpleRecyclerView;
import com.barryloh.mitechchallenge.viewmodels.DeliveryViewModel;
import com.barryloh.mitechchallenge.views.base.BaseFragment;

import butterknife.BindView;

/**
 * Created by barry on 15/05/2018.
 */

public class DeliveryFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, RecyclerViewCallbacks<Delivery> {

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.list_deliveries)
    SimpleRecyclerView listDeliveries;

    @BindView(R.id.cont_empty)
    LinearLayout contEmpty;

    // Holds reference to the VM that was initialised in MainActivity.
    private DeliveryViewModel viewModel;

    public static DeliveryFragment newInstance(DeliveryViewModel viewModel) {
        DeliveryFragment fragment = new DeliveryFragment();
        fragment.setViewModel(viewModel);
        return fragment;
    }

    @Override
    protected boolean isRetainingInstance() {
        // Retain this Fragment instance as it is being used in all layouts!
        return true;
    }

    @Override
    protected int inflateLayout() {
        return R.layout.fragment_delivery;
    }

    @Override
    protected String setToolbarTitle() {
        return getString(R.string.app_delivery_list);
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        swipeRefreshLayout.setOnRefreshListener(this);

        // Initialize the View Model and obtain the delivery list.
        viewModel.bindToRecyclerView(listDeliveries);

        // Initialize the adapter attached to the Recycler View.
        DeliveryAdapter adapter = new DeliveryAdapter(context, this);
        listDeliveries.setAdapter(adapter);

        // Initialize list with local database. If empty, obtain from server.
        if (viewModel.initializeDeliveriesFromLocal()) {
            swipeRefreshLayout.setRefreshing(true);
            onRefresh();
        }
    }

    @Override
    public void onItemClick(View view, Delivery data) {
        // Makes a call to the host activity to manage the on click logic.
        // Note: `getActivity()` has to be called here so that it'll get the latest host Activity.
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).onSelectDeliveryInfo(data);
        }
    }

    @Override
    public void onRefresh() {
        viewModel.getAllDeliveries();
    }

    /**
     * Manages layout state when an empty response is received from server.
     */
    public void onEmptyResponse() {
        showEmpty();
    }

    /**
     * Manages layout when the network call is successful.
     */
    public void onNetworkSuccess() {
        showList();
        swipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Manages layout when the network call has failed.
     */
    public void onNetworkFailure() {
        swipeRefreshLayout.setRefreshing(false);
        if (listDeliveries.getAdapter().getItemCount() == 0) {
            showEmpty();
        } else {
            showList();
        }
    }

    /**
     * Sets the VM for this Fragment to hold reference to it.
     * @param viewModel
     */
    public void setViewModel(DeliveryViewModel viewModel) {
        this.viewModel = viewModel;
    }

    /**
     * Shows the empty container and hides the list.
     */
    private void showEmpty() {
        listDeliveries.setVisibility(View.GONE);
        contEmpty.setVisibility(View.VISIBLE);
    }

    /**
     * Shows the list and hides everything else.
     */
    private void showList() {
        contEmpty.setVisibility(View.GONE);
        listDeliveries.setVisibility(View.VISIBLE);
    }

}
