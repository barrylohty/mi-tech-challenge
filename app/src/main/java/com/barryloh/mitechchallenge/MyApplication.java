package com.barryloh.mitechchallenge;

import android.app.Application;

import com.barryloh.mitechchallenge.database.config.DatabaseManager;

import io.realm.Realm;

/**
 * Overrides the default `Application` class.
 * Mostly used for initialization of modules, like the Realm database.
 *
 * Created by barry on 15/05/2018.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize Realm and its configurations here.
        Realm.init(this);
        DatabaseManager.initializeRealmConfiguration(this);

    }

}
