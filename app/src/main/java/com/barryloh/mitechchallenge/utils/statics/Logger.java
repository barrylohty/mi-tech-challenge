package com.barryloh.mitechchallenge.utils.statics;

import android.util.Log;

/**
 * Created by barry on 15/02/2018.
 */

public class Logger {

    public static final String TAG = "MITC";

    public static void LOGD(String message) {
        if (message == null) {
            return;
        }

        Log.d(TAG, message);
    }

    public static void LOGI(String message) {
        if (message == null) {
            return;
        }

        Log.i(TAG, message);
    }

    public static void LOGE(String message) {
        if (message == null) {
            return;
        }

        Log.e(TAG, message);
    }

    public static void LOGW(String message) {
        if (message == null) {
            return;
        }

        Log.w(TAG, message);
    }

}
