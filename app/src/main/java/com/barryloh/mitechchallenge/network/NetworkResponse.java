package com.barryloh.mitechchallenge.network;

import com.barryloh.mitechchallenge.callbacks.NetworkCallbacks;

import retrofit2.Call;
import retrofit2.Callback;

import static com.barryloh.mitechchallenge.utils.statics.Logger.LOGD;
import static com.barryloh.mitechchallenge.utils.statics.Logger.LOGE;


/**
 * Created by barry on 20/04/2018.
 */

public class NetworkResponse<T> implements Callback<T> {

    private final APIEndpoint endpoint;
    private NetworkCallbacks<T> callback;

    public NetworkResponse(APIEndpoint endpoint, NetworkCallbacks callback) {
        this.endpoint = endpoint;
        this.callback = callback;
    }

    @Override
    public void onResponse(Call<T> call, retrofit2.Response<T> response) {
        // If successful, goes here.
        if (response.isSuccessful()) {
            if (response.body() == null) {
                sendFailureCallback(500, "Empty response object");
                return;
            }

            sendSuccessCallback(response);
            return;
        }

        int responseCode = response.code();

        // If response code is not "OK", or "CREATED"
        if (responseCode != 200 || responseCode != 201) {
            if (response.errorBody() != null) {

                // TODO: Decode the error body here and return back to parent method call.
                // No error body is found or have been noticed. If there is and not handled yet,
                // just return a generic message to parent method call.
                sendFailureCallback(responseCode, "Error message not found");
                LOGD("Error message body: " + response.errorBody().toString());

            } else {
                sendFailureCallback(responseCode, "Error message not found");
            }

            return;
        }

        sendFailureCallback(responseCode, "Error message not found");
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        sendFailureCallback(0, t.toString());
    }

    private void sendSuccessCallback(retrofit2.Response<T> response) {
        LOGD("|" + endpoint.toString() + "| Success!");
        if (callback != null) {
            callback.onNetworkSuccess(endpoint, response.body());
        }
    }

    private void sendFailureCallback(int responseCode, String message) {
        LOGE("|" + endpoint.toString() + "| Error: " + message);
        if (callback != null) {
            callback.onNetworkFailure(endpoint, responseCode, message);
        }
    }

}
