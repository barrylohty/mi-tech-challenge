package com.barryloh.mitechchallenge.viewmodels.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.barryloh.mitechchallenge.callbacks.NetworkCallbacks;
import com.barryloh.mitechchallenge.network.API;
import com.barryloh.mitechchallenge.network.APIEndpoint;
import com.barryloh.mitechchallenge.network.APIInterface;
import com.barryloh.mitechchallenge.network.NetworkResponse;

import retrofit2.Call;

/**
 * Created by barry on 15/05/2018.
 */

public class BaseViewModel {

    protected Context context;
    protected NetworkCallbacks callback;

    protected RecyclerView recyclerView;

    public BaseViewModel(Context context, NetworkCallbacks callback) {
        this.context = context;
        this.callback = callback;
    }

    protected APIInterface getAPIClient() {
        return API.generate(context);
    }

    protected void enqueueCall(Call apiCall, APIEndpoint endpoint) {
        apiCall.enqueue(new NetworkResponse(endpoint, callback));
    }

    /**
     * Temporarily hold reference to the RecyclerView.
     * @param recyclerView
     */
    public void bindToRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

}
