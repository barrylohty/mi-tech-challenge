package com.barryloh.mitechchallenge.views;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.barryloh.mitechchallenge.R;
import com.barryloh.mitechchallenge.database.models.Delivery;
import com.barryloh.mitechchallenge.utils.statics.Keys;
import com.barryloh.mitechchallenge.views.base.BaseActivity;

import org.parceler.Parcels;

/**
 * Created by barry on 15/05/2018.
 */

public class DeliveryDetailsActivity extends BaseActivity {

    private Delivery delivery;

    // Immutable static tags used to reference the fragments.
    private static final String TAG_FRAGMENT_DELIVERY_DETAILS = "DELIVERY_DETAILS";

    @Override
    protected int setContentViewLayout() {
        return R.layout.activity_delivery_details;
    }

    @Override
    protected String setToolbarTitle() {
        return getString(R.string.app_delivery_details);
    }

    @Override
    protected void getArguments(Bundle args) {
        delivery = Parcels.unwrap(args.getParcelable(Keys.BUNDLE_DELIVERY));
    }

    @Override
    protected void init(Bundle savedInstanteState) {
        DeliveryDetailsFragment deliveryDetailsFragment = DeliveryDetailsFragment.newInstance(delivery);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_details, deliveryDetailsFragment, TAG_FRAGMENT_DELIVERY_DETAILS);
        fragmentTransaction.commit();
    }

}
