package com.barryloh.mitechchallenge.views.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.barryloh.mitechchallenge.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by barry on 23/02/2018.
 */

public abstract class BaseFragment extends Fragment {

    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    // Holds reference to the Activity's context.
    protected Activity context;

    /**
     * Required method to implement that specifies which layout to inflate into view.
     * @return Layout to inflate
     */
    abstract protected int inflateLayout();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        setRetainInstance( isRetainingInstance() );
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(inflateLayout(), container, false);
        ButterKnife.bind(this, view);
        initToolbar(view);
        init(view, savedInstanceState);

        return view;
    }

    private void initToolbar(View view) {
        toolbar = view.findViewById(R.id.toolbar);

        if (toolbar != null) {
            toolbar.setTitle(setToolbarTitle());
        }
    }

    /**
     * Override method to set a title to toolbar.
     * @return
     */
    protected String setToolbarTitle() {
        return "";
    }

    /**
     * Override method to perform additional initial configurations.
     * @param view
     * @param savedInstanceState
     */
    protected void init(View view, Bundle savedInstanceState) {}

    /**
     * Override this method to determine whether to retain this Fragment's instance.
     * This is especially useful when managing the Activity's state during change in orientation.
     * @return
     */
    protected boolean isRetainingInstance() {
        return false;
    }

}
