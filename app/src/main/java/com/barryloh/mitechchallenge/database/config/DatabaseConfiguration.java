package com.barryloh.mitechchallenge.database.config;

import com.barryloh.mitechchallenge.database.migration.DatabaseMigration;

import io.realm.RealmConfiguration;

/**
 * Created by barry on 20/03/2018.
 */

public class DatabaseConfiguration {

    private static final String NAME = "mitc_deliveries.realm";
    private static final int VERSION = 1;

    private static RealmConfiguration realmConfiguration;

    public static RealmConfiguration generate() {
        if (realmConfiguration == null) {
            realmConfiguration = new RealmConfiguration.Builder()
                    .name(NAME)
                    .schemaVersion(VERSION)
                    .migration(new DatabaseMigration())
                    .build();
        }

        return realmConfiguration;
    }

}
