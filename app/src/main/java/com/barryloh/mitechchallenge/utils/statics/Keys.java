package com.barryloh.mitechchallenge.utils.statics;

/**
 * Created by barry on 15/05/2018.
 */

public class Keys {

    public static final String DESCRIPTION = "description";
    public static final String CREATED_AT = "createdAt";

    public static final String BUNDLE_DELIVERY = "delivery";

}
