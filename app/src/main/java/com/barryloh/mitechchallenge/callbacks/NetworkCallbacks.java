package com.barryloh.mitechchallenge.callbacks;

import com.barryloh.mitechchallenge.network.APIEndpoint;

/**
 * Created by barry on 15/05/2018.
 */

public interface NetworkCallbacks<T> {

    void onNetworkSuccess(APIEndpoint endpoint, T response);
    void onNetworkFailure(APIEndpoint endpoint, int responseCode, String errorMessage);

}
